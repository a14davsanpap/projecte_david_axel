<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Imports del css -->    
    <link rel="stylesheet" type="text/css" href="css/style-general.css">
    <link rel="stylesheet" type="text/css" href="css/botons.css">
    <link rel="stylesheet" type="text/css" href="css/afegir.css">
    
  </head>
  <body>
    <header>
    	<a href="javascript:window.history.back();"><img id="atras" src="img/arrow.png" ></a>
         <a href="login.php"> <img id="logout" src="img/logout.png"></a>
     	 <h1 class="home">Agenda Online</h1>
    </header>
    <article>
    <div class="botons">
        <a href="consulta.php"><img id="icono-contacte" src="img/account-box.png" title="Agenda"></a>
        <a href="afegir.php"> <img id="icono-afegir" src="img/plus-circle.png" title="Afegir"></a>
        <a href="esborrar.php"> <img id="icono-eliminar" src="img/close-circle.png" title="Eliminar"></a>
      </div>
    <div class="general">
      <div id="titol_afegir">
        <h2>Afegir Contacte</h2>
      </div>
      <div id="contacte_afegir">
      <!-- Formulari, enviar Nom, Cognoms, Telefon, Data de naixement, botó per enviar i per fer reset de les dades -->
        <form action="afegirContacte.php" method="POST">
        
          <input type="text" name="firstname" placeholder="Nom">
          <br>
          <input type="text" name="lastname"placeholder="Cognom">
          <br>
          <input type="text" name="telefon"placeholder="Telefon">
          <br>
          <input type="text" name="inCalendari"placeholder="Data de Neixament"onclick="desplegaIframe()">
          <!-- Desplegament de iframe, selecció de dia, mes i any -->
          <iframe class="iframeClass" name="iframe" src="calendari.html" scrolling="no" frameborder="0"></iframe>
          <div id="botons_afegir">
        			<input class="button1" Type="image" src="img/save.png" name="submit"/>
    		</div>
        </form>
      </div>
      
    </div>	
    <footer>
      <div id = "footercontacts">
       Copyright © 2016
      </div>
    </footer>
    <script type="text/javascript">
    	function desplegaIframe() {
    		document.getElementsByClassName("iframeClass")[0].style.display="block";
    		document.getElementsByClassName("general")[0].style.height="493px";
    	}
    </script>
    </body>
</html>