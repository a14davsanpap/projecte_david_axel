<?php
  include ("conexio.php");
  session_start();
  $id_user = $_SESSION['id_user'];
  $consulta = "select nom,cognom,telefon,data_naixement,id_contacte from contactes where id_usu =".$id_user.";";
  $resposta = mysqli_query($conexion, $consulta);
  $registreContacte = mysqli_fetch_array($conexion,$consulta);

?>

<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/style-general.css">
    <link rel="stylesheet" type="text/css" href="css/botons.css">
    <link rel="stylesheet" type="text/css" href="css/consulta.css">
    <link rel="stylesheet" type="text/css" href="css/esborrar.css">
    <script src="borrar.js"></script>  
  </head>
  <body>
    <header>
    <a href="javascript:window.history.back();"><img id="atras" src="img/arrow.png" ></a>
         <a href="login.php"> <img id="logout" src="img/logout.png"></a>
      <h1 class="home">Agenda Online</h1>
    </header>
    <div class="botons">
        <a href="consulta.php"><img id="icono-contacte" src="img/account-box.png" title="Agenda"></a>
        <a href="afegir.php"> <img id="icono-afegir" src="img/plus-circle.png" title="Afegir"></a>
        <a href="esborrar.php"> <img id="icono-eliminar" src="img/close-circle.png" title="Eliminar"></a>
        <form method="POST" action="buscador.php">
         <input id="search" type="text" name="search" placeholder="Cerca">
        </form>
    </div>
<div id="contenidor_taula">
  <?php
  echo "<form action='esborrarMultiple.php' method='post'>";
    echo "<table id='taula'>";
      echo "<tr>";
        echo "<th>Nom</th>";
        echo "<th>Cognom</th>";
        echo "<th>Telefon</th>";
        echo "<th>Data Naixement</th>";
        echo "<th></th>";
       
      echo "</tr>";
    while($row = mysqli_fetch_assoc($resposta)){
      echo "<tr>";
        echo "<td>".$row["nom"]."</td>";
        echo "<td>".$row["cognom"]."</td>";
        echo "<td>".$row["telefon"]."</td>";
        echo "<td>".$row["data_naixement"]."</td>";
        echo "<td><a href='javascript:void(0)' onclick='borrar(".$row['id_contacte'].")'><img id='imgDelete' src='img/delete.png'></a></td>";
       
      echo "</tr>";
    }
    echo "</table>";
    echo "</form>";
  ?>

</div>


    <footer>
     <div id = "footercontacts">
       Copyright © 2016
      </div>
    </footer>
    </body>
</html>