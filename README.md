Credencials de la base de dades:
  - usuari: a15axeloparn_axl
  - password: agenda
  - base de dades: a15axeloparn_agenda

Credencials usuaris:
  - Usuari1: axel  password: ausias
  - Usuari2: david password: ausias1
  - usuari3: alex  password: ausias3
  - Usuari4: adan  password: ausias4

Estat del projecte:
  - Login.
  - Mostrar tots els contactes distingint usuari.
  - Base de dades llesta.
  - Funciona el calendari.
  - Css acabat.
  - Les pàgines de esborrar, afegir i editar funcionen.

Preparació per utilitzar la base de dades:
 - Ves al terminal de sistema i escriu la comanda: mysql -u usuari -p (usuari serà el que tinguis a la teva base de dades), el sistema demanarà la teva contrasenya, introdueix-la.
- Obre l'arxiu sql.txt, introdueix les comandes sql en el terminal.
- Modifica l'arxiu conexio.php, modifica les variables, $servername, $username, $password, $dbname per fer-ho coincidir amb les teves dades de la base dades.